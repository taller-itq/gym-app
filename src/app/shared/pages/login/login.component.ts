import { Component, OnInit } from '@angular/core';
import { LoggingService } from '../../services/logging.service';
import { HttpClient } from '@angular/common/http';
import opentelemetry from '@opentelemetry/api';
import { Span } from '@opentelemetry/sdk-trace-web';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css',
})
export class LoginComponent implements OnInit {


  constructor(private logger: LoggingService, private http: HttpClient) {
    this.logger.trace('checking if this works');
    this.logger.debug('checking if this works');
    this.logger.info('checking if this works');
    this.logger.warn('checking if this works');
    this.logger.error('checking if this works');
    this.logger.fatal('checking if this works');
  }

  ngOnInit() {
    this.http
      .get<any>('http://localhost:8080/api/jwt-svc/v1/issue')
      .subscribe({
        next: data => {
          this.logger.info("request completed")
          this.logger.info(data)
        },
        error: error => {
          this.logger.error("there was an error "+error.message)
        }
      });
  }
}
