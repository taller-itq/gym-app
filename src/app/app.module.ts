import { AppRoutingModule } from './app-routing.module';
// imports
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainHeaderComponent } from './shared/components/main-header/main-header.component';
import { MainFooterComponent } from './shared/components/main-footer/main-footer.component';
import { MainSidebarComponent } from './shared/components/main-sidebar/main-sidebar.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ContentWrapperComponent } from './shared/components/content-wrapper/content-wrapper.component';
import { LoginComponent } from './shared/pages/login/login.component';
import { MainComponent } from './shared/pages/main/main.component';
import { HttpClientModule } from '@angular/common/http';
import { LoggerModule, NGXLogger, NgxLoggerLevel } from 'ngx-logger';

// @NgModule decorator with its metadata
@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    LoginComponent,
    ContentWrapperComponent,
    MainHeaderComponent,
    MainFooterComponent,
    MainSidebarComponent,
  ],
  imports: [
    AppRoutingModule,
    CommonModule,
    RouterModule,
    BrowserModule,

    HttpClientModule,
    LoggerModule.forRoot({
      level: NgxLoggerLevel.DEBUG,
      disableConsoleLogging: false,
      serverLoggingUrl: '/loki/api/v1/push',
    })

  ],
  providers: [
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
